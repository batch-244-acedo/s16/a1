// Activity
	console.log("The result of num1 + num2 should be 30");
	let num1 = 25;
	let num2 = 5;
	let sum = num1 + num2;
	console.log("Actual Result: ");
	console.log(sum);

	console.log("The result of num3 + num4 should be 200");
	let num3 = 156;
	let num4 = 44;
	let sum1 = num3 + num4;
	console.log("Actual Result: ");
	console.log(sum1);

	console.log("The result of num5 - num6 should be 7");
	let num5 = 17;
	let num6 = 10;
	let diff = num5 - num6;
	console.log("Actual Result: ");
	console.log(diff);

	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;
	let	resultMinutes = (minutesHour * hoursDay) * daysYear;
	console.log("There are " + resultMinutes + " minutes in a year");

	let tempCelsius = 132;
	let resultFahrenheit = tempCelsius * (9/5) + 32;
	console.log(tempCelsius + " degrees Celsius when converted to Fahrenheit is " + resultFahrenheit);

	let divisor = 165;
	let dividend = 8;
	let num7 = divisor % dividend;
	console.log("The remainder of " + divisor + " divided by " + dividend + " is: " + num7);

	console.log("Is num7 divisible by 8?");
	console.log(num7 == 0);

	let divisor1 = 348;
	let dividend1 = 4;
	let num8 = divisor1 % dividend1;
	console.log("The remainder of " + divisor1 + " divided by " + dividend1 + " is: " + num8);

	console.log("Is num8 divisible by 4?");
	console.log(num8 == 0);







